<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class MY_Controller extends CI_Controller {
    protected $data;
    protected $view;
    protected $layout;

    public function __construct()
    {
        parent::__construct();
        // Language Selection
        $this->data['language'] = $this->uri->segment(1);
        if ( empty($this->data['language']) )
        {
            redirect('pt/home');
        }
        switch ( $this->data['language'] ) {
            case 'pt':
                $this->load->language('front_end', 'portuguese');
            break;
            case 'en':
                $this->load->language('front_end', 'english');
            break;
            default:
                $this->load->language('front_end', 'portuguese');
            break;
        }

        // Get categories for the menu
        $this->data['categories'] = $this->category_model->get_many_by(
            array(
                'parent' => '0',
                'status' => 'active'
            )
        );

        // Define basic assets
        $this->data['assets'] = array(
            js('js/jquery-1.7.1.js'),
            js('js/dropdown.js'),
            js('js/app.js')
        );

        // Define General Presenter
        require_once APPPATH . 'presenters/general_presenter.php';
            $this->data['layout_presenter'] = new General_presenter();

        // Define Category Presenter
        require_once APPPATH . 'presenters/category_presenter.php';
            $this->data['categories_top'] = new Category_presenter(
            $this->category_model->get_many_by( array('status' => 'active') ),
            $this->data['language']
        );

        // Base Breadcrumb
        $this->breadcrumb->append_crumb('Home', site_url($this->data['language'].'/home'));
    }

    /**
     * Remap the CI request, running the method
     * and loading the view
     */
    public function _remap($method, $arguments) {
        if (method_exists($this, $method)) {
            call_user_func_array(array($this, $method), array_slice($this->uri->rsegments, 2));
        } else {
            show_404(strtolower(get_class($this)).'/'.$method);
        }
        $this->_load_view();
    }

    /**
     * Load a view into a layout based on
     * controller and method name
     */
    private function _load_view() {
        // Back out if we've explicitly set the view to FALSE
        if ($this->view === FALSE) { return; }

        // Get or automatically set the view and layout name
        $view = ($this->view !== null) ? $this->view . '.php' : $this->router->directory . $this->router->class . '/' . $this->router->method . '.php';
        $layout = ($this->layout !== null) ? $this->layout . '.php' : 'layouts/application.php';

        // Load the view into the data
        $this->data['yield'] = $this->load->view($view, $this->data, TRUE);

        // Display the layout with the view
        $this->load->view($layout, $this->data);
    }
}

class ADMIN_Controller extends CI_Controller{

    function __construct() {
        parent::__construct();
        // Login
        $this->load->library('ion_auth');
        if (!$this->ion_auth->logged_in())
        {
            redirect('admin/login');
        }
        else{
            if (!$this->ion_auth->is_admin())
            {
                redirect('admin/login');
            }
        }
        //Load Language
        $this->lang->load('admin_base', 'english');
        //User info
        $this->data['logged_user'] = $this->ion_auth->user()->result();
   }
}